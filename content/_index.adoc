---
title: Full KLC
---

:toc: macro
:toclevels: 3
toc::[]

**KLC is suspended until further notice. Rules are currently being revised for KiCad version 6. No version 5 library additions will be accepted.**

**link:/history/[Revision History]**

---

The KiCad Library Convention (KLC) is a set of requirements for contributing to the official KiCad libraries. Users wishing to submit or update library files should be familiar with these guidelines.

The KLC are a set of __guidelines__, rather than __rules__. Electronic component libraries are diverse and complex, and exceptions can be made at the discretion of the library team.

Where the KLC deviates from a particular datasheet or manufacturer recommendation, the datasheet __should take preference__ unless there is a good reason not to do so (which should be clarified by a librarian).

Where the KLC is unclear, users should attempt to match the convention of existing library components, or seek further clarification.

Refer to the link:https://kicad.org/libraries/contribute/[contribution guidelines] for introductory information on contributing to the KiCad libraries.

---

== KLC Helper Scripts

The KiCad library team has developed a set of link:https://gitlab.com/kicad/libraries/kicad-library-utils[Python scripts] which can be used to help test if library components conform to the KLC requirements.

When a merge request is made to the libraries, the contributed files are automatically checked using these scripts. It can be helpful to run these scripts on your local machine before submitting a PR, as it will help speed up the process of merging your contribution(s) into the library.

To run the footprint checker script, for instance, `cd` into the "kicad-library-utils/pcb" directory, then run the Python script `./check_kicad_mod.py path_to_fp1.kicad_mod path_to_fp2.kicad_mod -vv`. This will carefully check your footprint according to the KLC requirements laid out below, notifying you of any discrepancies, errors, or violations. See more usage examples in the readme at the link above.

__Note: While many of the KLC guidelines are checked by these scripts, there are some which are not covered. Additionally, any PR requires manual checking by a member of the library team.__

---

== General Library Guidelines

The general library guidelines apply to all library elements (symbols / footprints / models / templates / 3D models). However, these guidelines may be overridden in some cases by specific exceptions described in further sections.

{{< klc_list title="General Guidelines" section="general" filter="G1.">}}

{{< klc_list title="Generic and Fully Specified Symbols" section="general" filter="G2." >}}

---

== Symbol Guidelines

The following guidelines apply to schematic symbols and symbol library files.

{{< klc_list title="Symbol Libraries" section="symbol" filter="S1.">}}

{{< klc_list title="Symbol Naming" section="symbol" filter="S2.">}}

{{< klc_list title="General Symbol Requirements" section="symbol" filter="S3.">}}

{{< klc_list title="Pin Requirements" section="symbol" filter="S4.">}}

{{< klc_list title="Footprint Association" section="symbol" filter="S5.">}}

{{< klc_list title="Symbol Metadata" section="symbol" filter="S6.">}}

{{< klc_list title="Special Symbols" section="symbol" filter="S7.">}}

---

== Footprint Guidelines

The following guidelines apply to PCB footprints and footprint libraries.

{{< klc_list title="Footprint Libraries" section="footprint" filter="F1.">}}

{{< klc_list title="General Footprint Naming Guidelines" section="footprint" filter="F2.">}}

{{< klc_list title="Specific Footprint Naming Guidelines" section="footprint" filter="F3.">}}

{{< klc_list title="General Footprint Requirements" section="footprint" filter="F4.">}}

{{< klc_list title="Layer Requirements" section="footprint" filter="F5.">}}

{{< klc_list title="Surface Mount Components" section="footprint" filter="F6.">}}

{{< klc_list title="Through Hole Components" section="footprint" filter="F7.">}}

{{< klc_list title="Virtual Components" section="footprint" filter="F8.">}}

{{< klc_list title="Footprint Properties" section="footprint" filter="F9.">}}

---

== 3D Model Guidelines

The following guidelines apply to contribution of 3D model data.

{{< klc_list title="Contributing Models" section="model" filter="M1.">}}

{{< klc_list title="3D File Requirements" section="model" filter="M2.">}}

---

== KLC Revision History

Revision information for the KLC can be found link:/libraries/klc/history/[here].
