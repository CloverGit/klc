+++
title = "F2.1 封装命名通用参考规范"
+++

每一个封装都是一个 `.kicad_mod` 文件 (存储在 `.pretty` 目录中)。命名规范主要取决于封装 _类型_，下面提供了一份通用参考:

. 特殊的封装类型放在前面，例如:
  * `QFN` - 方形扁平无引脚封装
  * `C` - 电容
. 封装名称与引脚数之间使用连字符 (`-`) 隔开:
  * `TO-90`
  * `QFN-48`
  * `DIP-20`
. 带特殊焊盘的封装，将特殊焊盘的标识字段放到引脚数字段之后，并用连字符 (`-`) 隔开，标识字段要求如下:
  * 该字段需包含不同类型的特殊焊盘的数目 `[count]`
  ** `[count]EP` - 针对裸露焊盘的命名格式 (器件底部的散热焊盘) 
  ** `[count]SH` - 针对屏蔽外壳的命名格式 (Unless such a pin is already expected for the part. An example would be a HDMI connector.)
  ** `[count]MP` - 仅用于安装或者固定器件的焊盘的命名格式
  * 示例如下:
  ** `DFN-6-1EP_2x2mm_P0.5mm_EP0.61x1.42mm`
  ** `Samtec_LSHM-110-xx.x-x-DV-S_2x10-1SH_P0.50mm_Vertical`
  ** `Molex_PicoBlade_53261-0271_1x02-1MP_P1.25mm_Horizontal`
. 封装名称中独立的 _字段_ (参数) 使用连字符 (`-`) 隔开。
. 封装尺寸统一使用 `长度` x `宽度` 的格式 (`高度` 参数可选):
  * `3.5x3.5x0.2mm`
  * `1x1in`
  * 为了使表达更加清晰明了，在必要的情况下可能会以字母 `B` 作为封装尺寸的前缀:
. 管脚排布:
  * `1x10`
  * `2x15`
. 器件的脚距以字母 `P` 作前缀:
  * `P1.27mm` - 1.27mm pitch
  * `P5.0mm` - 5.0mm pitch
. 封装仅在标准封装的基础上，对某些参数值进行了微调，需要如下注明:
  * `Drill1.25mm`
  * `Pad2.4x5.2mm`
. 安装方向例如: `Horizontal`, `Vertical`
. 对原始封装所做的任何修改，均以附加修改原因参数表示。
  * `_HandSoldering`
  * `_ThermalVias`
  * `_CircularHoles`

对于某些特殊的封装，并非都是严格要求使用上面所定义的字段，可以根据需要添加其他字段。
