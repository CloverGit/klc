+++
title = "S3.2 Text fields should use a common text size of 50mils"
+++

. All text fields (pin name, pin number, value, reference, footprint, datasheet) should use a text size of `50mil` (`1.27mm`).
. Pin names and pin numbers can use a text size as small as 20mil if the symbol is small or has special geometry.
